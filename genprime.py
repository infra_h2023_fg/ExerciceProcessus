#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sympy
import sys

if len(sys.argv) < 2:
    print("Il faut fournir le premier parametre du chemin du fichier. les nombres premiers y seront enregistrés.")
    sys.exit(1)

# Le nom du fichier est le premier paramètre.
nom_fichier = sys.argv[1]


compteur = 0
while True:
    compteur += 1

    # Enregistrement du prochain nombre premier dans le fichier en paramètre
    fichier = open(nom_fichier, 'a')
    fichier.write('%d\n' % sympy.prime(compteur))
    fichier.close()


