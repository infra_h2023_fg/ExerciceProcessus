#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from multiprocessing import Process
import os
import time
import sympy
import sys

if len(sys.argv) < 3:
    print("Il faut fournir deux paramètres: les 2 fichiers où les nombres premiers seront enregistrés.")
    sys.exit(1)

# Le nom du fichier est le premier paramètre.
nom_fichier1 = sys.argv[1]
nom_fichier2 = sys.argv[2]

def enfant(fich):
    print()
    print ('Démarrage le processus enfant avec le PID: ', os.getpid())
    print ('Processus parent est:', os.getppid())
    compteur = 0
    while True:
        compteur += 1

        # Enregistrement du prochain nombre premier dans le fichier en paramètre
        fichier = open(fich, 'a')
        fichier.write('%d\n' % sympy.prime(compteur))
        fichier.close()


if __name__ == '__main__':
    print()
    print ("Processus parent avec le PID: ", os.getpid())
    p = Process(target=enfant, args=(nom_fichier1, ))
    p.start()
    print ("Exécution du processus parent après le processus enfant")
    # p.join()
    print ("Démarrage le processus parent avec le PID: ", os.getpid())
    print ("Processus parent du parent: ", os.getppid())
    compteur = 0
    while True:
        compteur += 1

        # Enregistrement du prochain nombre premier dans le fichier en paramètre
        fichier = open(nom_fichier2, 'a')
        fichier.write('%d\n' % sympy.prime(compteur))
        fichier.close()

