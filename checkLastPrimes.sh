threshold=10000

while true; do
	b=$(tail -n 1 $1)
	a=$(tail -n 1 $2)
	((diff = $a - $b ))
	echo "current diff: $diff ($a vs $b)"
	if (($a - $b > $threshold)) || (($b - $a > $threshold)); then
		echo "We found a difference greater than $threshold"
		exit 0
	fi
	sleep 2
done
